<?php get_header(); ?>

<div class="container">

<div class="row">
	<div class="col-md-12">
	&nbsp;
	</div>
</div>

<div class="row">

	<div class="col-md-9">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			 
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>
	
	<div class="col-md-3">
		<?php if ( is_active_sidebar( 'home_sidebar' ) ) : ?>
			<?php dynamic_sidebar( 'home_sidebar' ); ?>
		<?php endif; ?>
	</div>

</div>

<br /><br />

</div>
	
<?php get_footer(); ?>