<?php get_header(); ?>

<div class="container">

<div class="row">
	<div class="col-md-12">
	&nbsp;
	</div>
</div>

<?php
	$cats = get_the_category(); 
	$cat = (array)$cats[0];
?>

<div class="row single-entry">

<?php
	if($cat['slug'] === "einsaetze" || $cat['slug'] === "nachrichten" ) {
		echo '<div class="col-md-12 content">';
	}
	else {
		echo '<div class="col-md-9 content">';
	}
?>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			<?php
					if($cat['slug'] === "einsaetze") {
						echo get_the_date("j.n.Y, G:i") . ' Uhr'; 
					}
					else {
						echo get_the_date("j.n.Y"); 
					}
			?>
				| <a href="<?php echo get_term_link($cat['term_id']); ?>"><?php echo $cat['name']; ?></a>
				<br /><br />
			 
			<?php the_content(); ?>
			
			<?php $url_link = rawurlencode(get_the_permalink()); ?>
			<br /><br />
			<div class="btn-group share-btn" role="group" aria-label="In sozialen Medien teilen">
			  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url_link; ?>" target="_blank"><button type="button" class="btn btn-outline-secondary btn-sm"><i class="icon-facebook"></i>Teilen</button></a>
			  <a href="https://twitter.com/share?url=<?php echo $url_link; ?>&text=<?php echo urlencode('Freiwillige Feuerwehr Schieder-Schwalenberg: ' . get_the_title()); ?>&via=flolip15" target="_blank"><button type="button" class="btn btn-outline-secondary btn-sm"><i class="icon-twitter"></i>Twittern</button></a>
			  <a href="whatsapp://send?text=<?php echo rawurlencode('Freiwillige Feuerwehr Schieder-Schwalenberg: ' . get_the_title()) . ' '; ?><?php echo $url_link; ?>" target="_blank"><button type="button" class="btn btn-outline-secondary btn-sm"><i class="icon-whatsapp"></i>WhatsApp</button></a>
			  <a href="mailto:?body=<?php echo $url_link; ?>&subject=<?php echo rawurlencode('Freiwillige Feuerwehr Schieder-Schwalenberg: ' . get_the_title()); ?>" target="_blank" title="Per E-Mail versenden" aria-label="Per E-Mail versenden"><button type="button" class="btn btn-outline-secondary btn-sm"><i class="icon-mail"></i> E-Mail</button></a>
			</div>
			<br /><br />
			<p><small>Dieser Beitrag wurde ungef&auml;hr <?php echo do_shortcode("[CPD_READS_THIS]"); ?> Mal aufgerufen.</small></p>
			<hr>
			<p class="text-center"><a href="#top">Zum Seitenanfang springen</a></p>
						
			<div class="row">
				<div class="col-md-6">
					<?php next_post_link('Neuer: %link'); ?>
				</div>
				<div class="col-md-6 text-right">
				   <?php previous_post_link('&Auml;lter: %link'); ?>
				</div>
			</div>

		<?php endwhile; endif; ?>
	</div>
	
	<div class="col-md-3">
		<?php if ( is_active_sidebar( $cat['slug'] . '_sidebar' ) ) : ?>
			<?php dynamic_sidebar( $cat['slug'] . '_sidebar' ); ?>
		<?php endif; ?>
	</div>

</div>

<br /><br />

</div>
	
<?php get_footer(); ?>