<?php get_header(); ?>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			&nbsp;
		</div>
	</div>

	<div class="row">

		<div class="col-md-9">

			<?php 
				$cat_info = get_the_category(); 
				
				#echo '<pre>';
				#print_r($cat_info);
				#echo '</pre>';
			?>
			
			<?php echo '<h2>' . $cat_info[0]->name . '</h2>'; ?>
			<?php echo 'Insgesamt ' . $cat_info[0]->count . ' Einträge'; ?>
			
			<?php
				$args = array(
					'numberposts' => -1,
					'offset' => 0,
					'orderby' => 'post_date',
					'order' => 'DESC',
					'post_status' => 'publish',
					'category' => $cat_info[0]->cat_ID
				);
				
				$archive_transient = get_transient($cat_info[0]->slug . '_archive_transient');
				if($archive_transient == false) {
					$recent_posts = wp_get_recent_posts($args);
					set_transient($cat_info[0]->slug . '_archive_transient', $recent_posts, 3600);
				}
				else {
					$recent_posts = $archive_transient;
				}
				
				#echo '<pre>';
				#print_r($recent_posts);
				#echo '</pre>';
				
				$recent_year = 0000;
				$recent_month = 0;
				$months = array("Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember");
				?>
		
		<?php foreach($recent_posts as $mypost) { ?>
		
			<?php
				$post_year = date("Y", strtotime($mypost['post_date']));
				$post_month = date("m", strtotime($mypost['post_date']));
				if($post_year !== $recent_year) { echo '<a name="' . $post_year . '">&nbsp;</a><hr><h4>' . $post_year . '</h4>'; }
				if($post_month !== $recent_month) { echo '<br /><strong>' . $months[$post_month-1] . '</strong>'; }
			?>
			<div class="row">
				
				<div class="col-md-2">
					<?php echo date("j.", strtotime($mypost['post_date'])); ?>
				</div>
				
				<div class="col-md-9">
					<a href="<?php echo get_permalink($mypost['ID']); ?>"><?php echo $mypost['post_title']; ?></a>
				</div>
			
			</div>
			
			<?php $recent_year = $post_year; ?>
			<?php $recent_month = $post_month; ?>
			
		<?php } ?>
		
		<br /><br />
		</div>
		
		<div class="col-md-3">
			<?php if ( is_active_sidebar( $cat_info[0]->slug . '_sidebar' ) ) : ?>
				<?php dynamic_sidebar( $cat_info[0]->slug . '_sidebar' ); ?>
			<?php endif; ?>
		</div>


	</div>

</div>
	
<?php get_footer(); ?>