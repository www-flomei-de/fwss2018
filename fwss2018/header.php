<?php

# Taken from here: https://github.com/dupkey/bs4navwalker
# Register Custom Navigation Walker
require_once('bs4navwalker.php');


?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Freiwillige Feuerwehr Schieder-Schwalenberg">
    <link rel="icon" href="../../favicon.ico">

    <title><?php wp_title(' - ', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	
    <link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" as="style">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/css/fontello.min.css" as="style">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/style.min.css" as="style">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/droidsans-webfont.woff2" as="font">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/droidsans-bold-webfont.woff2" as="font">
	<link rel="preload" href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/fontello.woff2?25642644" as="font">
	
	<?php if(is_single()) : ?>
	
	<meta property="og:title" content="<?php echo get_the_title(); ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="<?php echo get_the_excerpt(); ?>" />
	<meta property="og:url" content="<?php echo get_permalink(); ?>" />
	<meta property="og:locale" content="de_DE" />
	<meta property="og:site_name" content="Feuerwehr Schieder-Schwalenberg" />
	<meta property="og:image" content="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'); ?>" />
	
	<?php endif; ?>

    <link href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/css/fontello.min.css" rel="stylesheet">
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/style.min.css" rel="stylesheet">	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	<nav class="navbar navbar-expand-lg">
		<div class="container">
		  <a class="navbar-brand" id="logo" href="<?php echo get_home_url(); ?>"><img alt="Freiwillige Feuerwehr Schieder-Schwalenberg" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" /></a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<i class="icon-menu"></i>
		  </button>
		  <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
			<?php
			   wp_nav_menu([
				 'menu'            => 'top',
				 'menu_class'      => 'navbar-nav',
				 'depth'           => 2,
				 'fallback_cb'     => 'bs4navwalker::fallback',
				 'walker'          => new bs4navwalker()
			   ]);
			   ?>
		  </div>
		</div>
	</nav>
	
	<?php get_weather_alerts(); ?>