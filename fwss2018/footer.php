<?php



?>

	</div>
	
	<!-- /container -->
	
	<footer>
		<div class="container">
			<!-- footer widgets -->
			<div class="row footer">
				<div class="col-md-4">
					<?php if ( is_active_sidebar( 'footer_left' ) ) : ?>
						<?php dynamic_sidebar( 'footer_left' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<?php if ( is_active_sidebar( 'footer_middle' ) ) : ?>
						<?php dynamic_sidebar( 'footer_middle' ); ?>
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<?php if ( is_active_sidebar( 'footer_right' ) ) : ?>
						<?php dynamic_sidebar( 'footer_right' ); ?>
					<?php endif; ?>
					
					<?php global $count_per_day;

					   echo '<small>Besucher heute: ' . $count_per_day->getUserToday(1) . '<br />';
					   echo 'gestern: ' . $count_per_day->getUserYesterday(1) . '<br />';
					   echo 'insgesamt: ' . $count_per_day->getUserAll(1) . '</small>';

					?>

				</div>
			</div>
			<!-- /footer widgets -->
		</div>
	</footer>

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
  </body>
</html>
