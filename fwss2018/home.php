<?php get_header(); ?>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			&nbsp;
		</div>
	</div>

	<div class="row news-entries">

		<div class="col-md-9">

		
		<?php
			$args = array(
				'numberposts' => 5,
				'offset' => 0,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'post_status' => 'publish'
			);

			$recent_posts = wp_get_recent_posts($args);
			
			#echo '<pre>';
			#print_r($recent_posts);
			#echo '</pre>';
		?>
		
		<?php foreach($recent_posts as $mypost) { ?>
		
			<div class="row news-entry">
				<div class="col-md-12 order-2 order-md-1">
					<h3><a class="headline-link" href="<?php echo get_permalink($mypost['ID']); ?>"><?php echo $mypost['post_title']; ?></a></h3>
				</div>
							
				<div class="col-md-8 order-3 order-md-2">
				<span class="entry-meta">
					<?php
						$cats = get_the_category($mypost['ID']); 
						$cat = (array)$cats[0];

						if($cat['slug'] === "einsaetze") {
							echo date("j.n.Y, G:i", strtotime($mypost['post_date'])) . ' Uhr'; 
						}
						else {
							echo date("j.n.Y", strtotime($mypost['post_date'])); 
						}
					?>&nbsp;|&nbsp;<a href="<?php echo get_term_link($cat['term_id']); ?>"><span class="category-name"><?php echo $cat['name']; ?></span></a>
				
				</span>
					
					<?php echo $mypost['post_excerpt']; ?>
					
					<a class="readmore-link" href="<?php echo get_permalink($mypost['ID']); ?>">Mehr lesen</a>
				</div>
				
				<div class="col-md-4 order-1 order-md-3">
					<img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($mypost['ID'], 'thumbnail'); ?>" />
				</div>
			
			</div>
			
		<?php } ?>
		
		<div class="row">
			<div class="col-md-12">
				<h4>Mehr Einsätze und Nachrichten?</h4>
				<p>In unserem Archiv finden Sie alle Beiträge die wir je veröffentlicht haben.</p>
				<p>Sie sind nach den jeweiligen Kategorien sortiert: <a href="<?php echo get_term_link('einsaetze', 'category'); ?>">Einsätze,</a> <a href="<?php echo get_term_link('nachrichten', 'category'); ?>">Nachrichten,</a>  <a href="<?php echo get_term_link('jugendfeuerwehr', 'category'); ?>">Jugendfeuerwehr</a>  oder <a href="<?php echo get_term_link('musikzug', 'category'); ?>">Musikzug.</a></p>
				<br /><br />
			</div>
		</div>
		
		</div>
		<div class="col-md-3">
			<?php if ( is_active_sidebar( 'home_sidebar' ) ) : ?>
				<?php dynamic_sidebar( 'home_sidebar' ); ?>
			<?php endif; ?>
		</div>


	</div>

</div>
	
<?php get_footer(); ?>