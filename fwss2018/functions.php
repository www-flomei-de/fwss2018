<?php

# code is going here

# enable post thumbnails
add_theme_support( 'post-thumbnails' );


function register_theme_sidebars() {

	register_sidebar( array(
		'name'          => 'Sidebar auf Startseite',
		'id'            => 'home_sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	/*
	register_sidebar( array(
		'name'          => 'Einsätze Sidebar',
		'id'            => 'einsaetze_sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Nachrichten Sidebar',
		'id'            => 'nachrichten_sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	*/
	register_sidebar( array(
		'name'          => 'Musikzug Sidebar',
		'id'            => 'musikzug_sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Jugendfeuerwehr Sidebar',
		'id'            => 'jugendfeuerwehr_sidebar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );

}
add_action( 'widgets_init', 'register_theme_sidebars' );


function register_footer_sidebars() {

	register_sidebar( array(
		'name'          => 'Footer Links',
		'id'            => 'footer_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Footer Mitte',
		'id'            => 'footer_middle',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );
	register_sidebar( array(
		'name'          => 'Footer Rechts',
		'id'            => 'footer_right',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '',
		'after_title'   => '',
	) );

}
add_action( 'widgets_init', 'register_footer_sidebars' );


function get_weather_alerts() {
	
	$transient_warnings = get_transient('dwd_warnings');
	if($transient_warnings == false) {
		$warnings = file_get_contents('https://www.dwd.de/DWD/warnungen/warnapp/json/warnings.json');
		$warnings = jsonp_decode($warnings);
		$warn_arr = $warnings->warnings;
		set_transient('dwd_warnings', $warn_arr, 1800);
	}
	else {
		$warn_arr = $transient_warnings;
	}
	
	#echo '<pre>';
	#print_r($warn_arr);
	#echo '</pre>';

	$warning_exists = false;
	$warnings_text = '';
	
	foreach($warn_arr as $warn) {
		foreach($warn as $my_warning) {
			if($my_warning->regionName === 'Kreis Lippe') {
				$warning_exists = true;
				
				$dst = 3600;
				$warnings_text .= $my_warning->headline . ' vom ' . date("j.n.Y, G:i", ($my_warning->start/1000)+$dst) . ' Uhr bis ' . date("j.n.Y, G:i", ($my_warning->end/1000)+$dst) . ' Uhr<br />';
			}
		}			
	}
	if($warning_exists) {
		echo'<div class="container"><br /><br /><div class="alert alert-danger" role="alert">';
		echo $warnings_text;
		echo '<small><a href="https://www.dwd.de/DE/wetter/warnungen_landkreise/warnWetter_node.html?ort=Schieder-Schwalenberg" target="_blank">Weitere Informationen beim Deutschen Wetterdienst</a></small>';
		echo '</div></div>';
	}
}

# taken from: https://stackoverflow.com/questions/5081557/extract-jsonp-resultset-in-php
function jsonp_decode($jsonp, $assoc = false) { // PHP 5.3 adds depth as third parameter to json_decode
    if($jsonp[0] !== '[' && $jsonp[0] !== '{') { // we have JSONP
       $jsonp = substr($jsonp, strpos($jsonp, '('));
    }
    return json_decode(trim($jsonp,'();'), $assoc);
}


# disable menu items for people who could break them...
function remove_menus(){
	
	if(get_current_user_id() !== 1) {
	  remove_menu_page( 'edit.php?post_type=calendar' );
	  remove_menu_page( 'link-manager.php' );
	  remove_menu_page( 'edit-comments.php' );
	  remove_menu_page( 'plugins.php' );
	  remove_submenu_page( 'users.php', 'user-new.php' );
	  remove_submenu_page( 'users.php', 'users.php' );
	  remove_submenu_page( 'index.php', 'update-core.php' );
	  remove_submenu_page( 'upload.php', 'tiny-bulk-optimization' );
	  remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );
	  remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=category' );
	  remove_submenu_page( 'themes.php', 'themes.php' );
	  remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Findex.php' );
	  remove_menu_page( 'tools.php' );
	  remove_menu_page( 'options-general.php' );
	}
}
add_action( 'admin_menu', 'remove_menus' );

?>